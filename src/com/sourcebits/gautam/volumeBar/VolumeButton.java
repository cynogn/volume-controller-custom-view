package com.sourcebits.gautam.volumeBar;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

/**
 * VolumeButton draws a Volume bar and the volume on it.
 * 
 * @author Gautam
 * 
 * 
 */
public class VolumeButton extends View implements OnTouchListener {

	private boolean stateOn;// state of the Volume Button increate volume or
							// Decrease volume
	private static final String TAG = VolumeButton.class.getCanonicalName();
	private static Paint paint;
	private static final int COMPONENT_WIDTH = 50;// Default Component Width
	private static final int COMPONENT_HEIGHT = 50;// Default Component Width
	private int mWidth;// Volume Bar width center
	private int mHeight;// Volume Bar height center
	float center_x;// Volume Bar x center
	float center_y;// Volume Bar y center
	private static int K = -1;// Default volume value
	float radius = 90.f;// //Default radius value
	private static int MAX_VOLUME = 20; // Set maximum maximum volume
	private static int VOLUMEBAR_WIDTH = 30; // Set maximum maximum volume
	private static int VOLUME_SIZE = 35; // Set maximum maximum volume
	; // Set maximum maximum volume

	public VolumeButton(Context context) {
		super(context);
		paint = new Paint();// initialized paint
	}

	public VolumeButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		paint = new Paint();
		stateOn = true;// default value is increase
		setOnTouchListener(this);
	}

	@Override
	protected void onDraw(Canvas canvas) {

		center_x = mWidth / 2;// setting the width
		center_y = mHeight / 2;// setting the height
		final RectF oval = new RectF();
		oval.set(center_x - radius, center_y - radius, center_x + radius,
				center_y + radius);
		paint.setColor(Color.GRAY);
		canvas.drawCircle(center_x, center_y, 90.0f, paint);
		if (stateOn) {
			K++;
			paint.setColor(Color.CYAN);
			canvas.drawArc(oval, 0, 360 / MAX_VOLUME * K, true, paint);
			if (K == MAX_VOLUME) {
				paint.setColor(Color.RED);
				canvas.drawArc(oval, 360 / MAX_VOLUME * K, 10, true, paint);
			}
			paint.setColor(Color.WHITE);
			canvas.drawCircle(center_x, center_y, radius - VOLUMEBAR_WIDTH,
					paint);

		} else {
			K--;

			paint.setColor(Color.CYAN);
			canvas.drawArc(oval, 0, 360 / MAX_VOLUME * K, true, paint);

			paint.setColor(Color.WHITE);
			canvas.drawCircle(center_x, center_y, radius - VOLUMEBAR_WIDTH,
					paint);// drawing
			// the
			// volume
			// bar

		}
		paint.setColor(Color.CYAN);
		paint.setTextSize(VOLUME_SIZE);// setining the Volume on the canvas
		canvas.drawText(K + "", center_x, center_y, paint);

	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		mWidth = MeasureSpec.getSize(widthMeasureSpec);
		mHeight = MeasureSpec.getSize(heightMeasureSpec);

		int widthMode = MeasureSpec.getMode(widthMeasureSpec);
		int heightMode = MeasureSpec.getMode(heightMeasureSpec);

		mWidth = chooseDimensionWidth(widthMode, mWidth);
		mHeight = chooseDimensionHeight(heightMode, mHeight);

		Log.d(TAG, "width =" + mWidth + ", height =" + mHeight);
		setMeasuredDimension(mWidth, mHeight);
	}

	/*
	 * Returns the Height
	 */
	private int chooseDimensionWidth(int mode, int size) {
		if (mode == MeasureSpec.AT_MOST || mode == MeasureSpec.EXACTLY) {
			return size;
		} else { // (mode == MeasureSpec.UNSPECIFIED)
			return COMPONENT_WIDTH;
		}
	}

	/*
	 * Returns the width
	 */
	private int chooseDimensionHeight(int mode, int size) {
		if (mode == MeasureSpec.AT_MOST || mode == MeasureSpec.EXACTLY) {
			return size;
		} else { // (mode == MeasureSpec.UNSPECIFIED)
			return COMPONENT_HEIGHT;
		}
	}

	/*
	 * on clcik listener
	 * 
	 * @see android.view.View.OnTouchListener#onTouch(android.view.View,
	 * android.view.MotionEvent)
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int x = (int) event.getX();
		int y = (int) event.getY();
		Rect containingRect = new Rect(0, 0, (int) center_x, (int) center_y * 2);

		if (containingRect.contains(x, y)) {

			{
				if (K > 0) {
					stateOn = false;
					invalidate();
					Log.v("CLICKED", "false");
				}
			}
		} else {
			if (K < MAX_VOLUME) {
				stateOn = true;
				invalidate();
				Log.v("CLICKED", "True");
			}
		}

		return false;
	}
}
